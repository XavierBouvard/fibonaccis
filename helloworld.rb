#!/usr/bin/ruby
require 'enumerator'

# FIBONACCI ARRAY
def fibonacci_array(take)
    cur = 1
    prev = 0
    a = []
    take.times do 
        cur, prev = cur+prev, cur
        a << cur
    end
    return a
end

puts 'FIBONACCI ARRAY'
p fibonacci_array(10)

# FIBONACCI GENERATOR
fibonacci_generator = Enumerator.new do |y|
    cur, prev = 1, 0
    while true do
        cur, prev = cur+prev, cur
        y.yield cur
    end
end

# (1..10).each do  p fibonacci_generator.next end
puts 'FIBONACCI GENERATOR'
fibonacci_generator.take(10).each { |v| p v }

# FIBONACCI FUNCTIONAL
def fibonacci_functional 
    n = ->(cur, prev) {
        cur, prev = cur+prev, cur
        return cur, -> { n.call(cur, prev) }
      }
    return n.call(1, 0)
end

puts 'FIBONACCI FUNCTIONAL'
a, n = fibonacci_functional()
(1..10).each do 
    p a
    a, n = n.call()
end