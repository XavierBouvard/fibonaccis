#!/usr/bin/env python


# FIBONACCI ARRAY
def fibonacci_array(take):
    a = []
    cur, prev = 1, 0
    for _ in range(take):
        cur, prev = cur + prev, cur
        a.append(cur)
    return a


print('FIBONACCI ARRAY')
print(fibonacci_array(10))


# FIBONACCI GENERATOR
def fibonacci_gen():
    cur, prev = 1, 0
    while True:
        cur, prev = cur + prev, cur
        yield cur


print('FIBONACCI GENERATOR')
a = fibonacci_gen()
for i in range(10):
    print(next(a))


# FUNCTIONAL
def fibonacci():
    def next(cur, prev):
        cur, prev = cur+prev, cur
        return cur, lambda: next(cur, prev)
    return next(1, 0)


print('FIBONACCI FUNCTIONAL')
a, n = fibonacci()
for i in range(10):
    print(a)
    a, n = n()
